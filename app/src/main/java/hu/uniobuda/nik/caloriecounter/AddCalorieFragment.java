package hu.uniobuda.nik.caloriecounter;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;


public class AddCalorieFragment extends Fragment {

    private DatabaseReference dbr;
    private int plusCalYear;
    private int plusCalMonth;
    private int plusCalDay;
    private String fullDate;
    private AppCompatTextView textDailyDate;
    private AppCompatEditText addDailyCalorie;
    private AppCompatTextView textAddDailyCalorie;
    private AppCompatButton buttonAdd;
    private UserActivity act;
    private DatePickerDialog.OnDateSetListener dDateSetListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_calorie, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textDailyDate = view.findViewById(R.id.DailyDate);
        addDailyCalorie = view.findViewById(R.id.addDailyCalorie);
        textAddDailyCalorie = view.findViewById(R.id.textDailyCalorie);
        buttonAdd = view.findViewById(R.id.buttonAddCalorie);
        act = (UserActivity) getActivity();
        Calendar c = Calendar.getInstance();
        plusCalDay = c.get(Calendar.DAY_OF_MONTH);
        plusCalMonth = c.get(Calendar.MONTH);
        plusCalYear = c.get(Calendar.YEAR);
        fullDate = setDateFormat(plusCalYear,plusCalMonth,plusCalDay);
        textDailyDate.setText(fullDate);
        dbr = FirebaseDatabase.getInstance().getReference("userdatas/"+act.getFba().getCurrentUser().getUid()+"/daily_calorie");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        textDailyDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        act,
                        R.style.Theme_AppCompat_Light_Dialog_MinWidth,
                        dDateSetListener,
                        year,
                        month,
                        day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.GRAY));
                datePickerDialog.show();
            }
        });
        dDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                fullDate = setDateFormat(year,month,day);
                plusCalDay = day;
                plusCalMonth = month;
                plusCalYear = year;
                textDailyDate.setText(fullDate);
            }
        };
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PushCalorie();
            }
        });
    }

    private void PushCalorie(){

        DatabaseReference pushReference = dbr.child(fullDate).push();
        String actualKey = pushReference.getKey();
        DailyCalorie newCalorie = new DailyCalorie(fullDate,Float.parseFloat(addDailyCalorie.getText().toString()), actualKey, plusCalDay);
        String dateFullPushFormat = plusCalYear + "/" + plusCalMonth;
        dbr.child(dateFullPushFormat).child(actualKey).setValue(newCalorie);

    }

    private String setDateFormat(int year, int month, int day){
        return month + "/" + day + "/" + year;
    }
}

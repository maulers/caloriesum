package hu.uniobuda.nik.caloriecounter;


import android.content.Intent;

import android.os.Bundle;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadLoginFragment();
    }

    public void loadLoginFragment(){
        loadMainActivityFragment(R.id.menu,LoginFragment.class.getName(),new LoginFragment());
    }

    public void loadRegisterFragment(){
        loadMainActivityFragment(R.id.menu,RegisterFragment.class.getName(),new RegisterFragment());
    }
    public void loadUserActivity(){
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }
}

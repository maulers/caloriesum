package hu.uniobuda.nik.caloriecounter;

import java.util.Calendar;
import java.util.Date;

public class UserProfile {

    private float weight;
    private String uid;
    private String email;
    private int dobYear;
    private int dobMonth;
    private int dobDay;
    private String weightType;
    private float height;
    private String heightType;
    private int age;
    private String gender;

    private void init(){
        weight = 0;

    }

    public UserProfile() {
    }

    public UserProfile(String uid, String email) {
        this.uid = uid;
        this.email = email;
    }

    public float getWeight() {
        return weight;
    }

    public String getUid() {
        return uid;
    }

    public String getEmail() {
        return email;
    }

    public String getWeightType() {
        return weightType;
    }

    public float getHeight() {
        return height;
    }

    public String getHeightType() {
        return heightType;
    }

    public int getDobYear() {
        return dobYear;
    }

    public int getDobMonth() {
        return dobMonth;
    }

    public int getDobDay() {
        return dobDay;
    }

    public String getGender() {
        return gender;
    }



    public void setWeight(float weight) {
        this.weight = weight;
    }

    public void setWeightType(String weightType) {
        this.weightType = weightType;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setHeightType(String heightType) {
        this.heightType = heightType;
    }

    public void setDobYear(int dobYear) {
        this.dobYear = dobYear;
    }

    public void setDobMonth(int dobMonth) {
        this.dobMonth = dobMonth;
    }

    public void setDobDay(int dobDay) {
        this.dobDay = dobDay;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int aktkor= year- dobYear;
        return aktkor;
    }

    public void setAge(int age) {

        this.age =age;
    }
}

package hu.uniobuda.nik.caloriecounter;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;

import android.support.v7.widget.AppCompatTextView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class statistics extends android.support.v4.app.Fragment {




    private DatabaseReference dbr;
    private UserActivity act;
    private  LineChart chart;
    ArrayList<Entry> entries;
    Calendar currentTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        act = (UserActivity) getActivity();
        chart = view.findViewById(R.id.chart);

        entries = new ArrayList<Entry>();
        /*LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.invalidate();*/

        currentTime = Calendar.getInstance();
        int year  = currentTime.get(Calendar.YEAR);
        int month  = currentTime.get(Calendar.MONTH);
        dbr = FirebaseDatabase.getInstance().getReference("userdatas/"+act.getFba().getCurrentUser().getUid()+"/daily_calorie/"+Integer.toString(year)+"/"+Integer.toString(month));



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    DailyCalorie dc = snapshot.getValue(DailyCalorie.class);
                    entries.add(new Entry(dc.getDay(),Math.round(dc.getCalorie())));
                }
                LineDataSet dataSet = new LineDataSet(entries, "Napi bontás"); // add entries to dataset
                LineData lineData = new LineData(dataSet);
                chart.setData(lineData);
                chart.invalidate();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


}

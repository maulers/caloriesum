package hu.uniobuda.nik.caloriecounter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Creep-PC on 2018. 04. 02..
 */

public class RegisterFragment extends Fragment{
    private AppCompatEditText register_email;
    private AppCompatEditText register_password;
    private AppCompatButton register_button;
    private AppCompatTextView to_login;
    private DatabaseReference dbr;

    private FirebaseAuth fba;
    private MainActivity mainAct;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register,container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dbr = FirebaseDatabase.getInstance().getReference("userdatas");
        mainAct = (MainActivity) getActivity();

        register_email = view.findViewById(R.id.register_email);
        register_password = view.findViewById(R.id.register_password);
        register_button = view.findViewById(R.id.register_button);
        to_login = view.findViewById(R.id.register_to_login);

        fba = FirebaseAuth.getInstance();



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        to_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainAct.loadLoginFragment();
            }
        });

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
    }

    private void registerUser(){
        if(TextUtils.isEmpty(register_email.getText().toString())){
            Toast.makeText(mainAct, "Please enter your email", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(register_password.getText().toString())){
            Toast.makeText(mainAct, "Please enter your password", Toast.LENGTH_LONG).show();
            return;
        }
        fba.createUserWithEmailAndPassword(register_email.getText().toString(), register_password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(mainAct,"Registered Successfully", Toast.LENGTH_SHORT).show();
                    UserProfile newUser = new UserProfile(fba.getCurrentUser().getUid(),fba.getCurrentUser().getEmail());
                    dbr.child(fba.getCurrentUser().getUid()).child("profile").setValue(newUser);
                    mainAct.loadUserActivity();
                }else{
                    Toast.makeText(mainAct,"Could not register please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}

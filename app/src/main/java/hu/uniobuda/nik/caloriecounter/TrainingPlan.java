package hu.uniobuda.nik.caloriecounter;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;

import android.support.v7.widget.AppCompatTextView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.GenericArrayType;


public class TrainingPlan extends android.support.v4.app.Fragment {

    private int testindex;
    private AppCompatButton fat;
    private AppCompatButton powerinc;
    private AppCompatButton  cross;
    private  AppCompatTextView calorietext;
    private AppCompatTextView eterv;



    private UserActivity act;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_training_plan, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        act = (UserActivity) getActivity();
        float height= act.getUserProfile().getHeight();
        int weight= (int)(act.getUserProfile().getWeight());
        int kor= act.getUserProfile().getAge();
        String gender= act.getUserProfile().getGender();
        double kaloria= GenderCalorie(gender,kor,weight);

        fat=view.findViewById(R.id.FatBurn);
        cross=view.findViewById(R.id.Crossfit);
        powerinc=view.findViewById(R.id.WeightIncrease);
        calorietext = view.findViewById(R.id.Tindex);
        eterv=view.findViewById(R.id.Plan);

        calorietext.setText(Double.toString(kaloria)+ " Kcal");






        fat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eterv.setText(": A szálkásítás alatt a testépítők gyakorlatilag a zsírtömeg leadását értik. Amikor az izomtömegük eléri az általuk kívánt szintet, fogyás központú diétába kezdenek, hogy a tömegnövelés során felszedett zsírt leadják. Mivel az izomtömegük elég nagy, a tudásuk megfelelő, és az anyagcseréjük is gyors, ezért könnyen meg tudják ezt valósítani. Szálkásítás során a legnehezebb feladat megtartani az edzések során felépített izomzatot. Különösen nehéz ez az ún. ektomorf (kimondottan vékony) testalkatú férfiaknak és nőknek. Súlyzós edzésekkel és megfelelő, személyre szabott étrenddel a diéta alatti izomvesztés azonban minimalizálható. Azt viszont nagyon fontos tudni, hogy a tömegnövelés-szálkásítás felosztás haladó sportolóknál eredményes. Annak, aki most kezd el sportolni vagy még csak kacérkodik az életmódváltás gondolatával elsősorban az egészségügyi értékeit kell normalizálni, fel kell gyorsítani a – nagy valószínűséggel – lelassult anyagcserét, és ízületkímélő edzésekkel hozzá kell szoktatni a testet (a szívet, a tüdőt, a keringési rendszert, az ízületeket, az izmokat stb) a rendszeres edzésekhez.");
            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eterv.setText("Egyazon edzés, melyben súlyt emelsz, futsz és fekvőtámaszozol? Igen: ez CrossFit. A CrossFit nevű módszert néhány éve alakították ki Amerikában, és egy-egy elemét kiemelve semmi különös nincs benne, egyedi filozófiája miatt viszont egyre kedveltebb már itthon is. A CrossFittel olyan programot kell elképzelned, mely számtalan sportág mozgáskészletéből merít, például az atlétikából, a súlyemelésből, az evezésből, az úszásból és a tornából. Az edzés során nem csak egy-egy izomcsoportot, hanem az egész testet átmozgató gyakorlatokat végzel. Olyan izmokat erősít, amelyeket nem csak sportolás közben, de akár lépcsőzésnél, kisgyerek emelgetésnél, vagy házimunka közben is használsz, így a mindennapi élet is könnyebbé válik tőle. Hogy zajlik a CrossFit edzés? Az alapkoncepció: három nap edzés, egy nap pihenő. Az egyik jól bevált edzésminta az, hogy bemelegítés után egy súlyzós alapgyakorlatból 3-5x3-5 ismétlést végzünk közepes pihenőkkel, majd egy tízperces gimnasztikai (saját testtel végzett) gyakorlatsort gyors tempóban, és végül 2-10 perc magas intenzitású metabolikus (anyagcsere) kondícionálás zárja az edzést. Egy másik technika, hogy keverjük a súlyemelés és gimnasztikai elemeit, hogy drámai metabolikus kihívást érjünk el. Egy példa erre az, amikor közepesen nehéz 5 ismétléses guggolást maximális ismétlésszámú húzódzkodás követ azonnal. Ebből a párból 3-5 kört végzünk.");
            }
        });

        powerinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eterv.setText("A tömegnövelés a testépítés egyik alapjának tekinthető. Mikor valaki az edzés mellett dönt, akkor minden bizonnyal izomtömeget akar építeni, vagy zsírt szeretne égetni. Illetve van a kevésbé informált kategória (sajnos ezekből van több) akik úgy gondolják, hogy a kettő egyszerre is működik. Zsírt égetni és izomtömeget növelni egyszerre majdhogynem lehetetlen. Bár van néhány fehér holló, aki a testfelépítésének, genetikájának köszönhetően képes erre. Illene mostmár megmagyaráznom, hogy a két dolog együtt, miért közelít a lehetetlenhez. A tömegnövelési időszakban rengeteget kell enni. Bár itt is ügyelni kell arra, hogy milyen minőségű ételeket fogyasztunk, de a „legjobb” kaják is tartalmaznak zsírt. És mivel nagyon nagy mennyiségeket eszünk a tömegnövelés ideje alatt, ezért a zsír is szépen csendben hozzáadódik. Ezenkívül a szénhidrát is ott van, amiből diétás időszakban mérsékelten kell fogyasztanunk, tömegnövelésnél viszont nagy mennyiségre van szükség. A lényeg az, hogy tömegnövelési időszakban nagyon nehezen tudjuk elkerülni azt is hogy ne kerüljön fel némi „plusz” a száraz izomzaton kívül, nemhogy még a zsírégetést is kitűzzük célul, ezzel egyetemben. Most azonban tisztázzuk azt, hogy mi is pontosan a tömegnövelés? Mi az a tömegnövelés? A tömegnövelés azon időszak a testépítésen belül, amikor izomtömeget szeretnénk felépíteni úgy, hogy emellett ne zsírosodjunk el.");
            }
        });


    }
public double GenderCalorie(String gender,int kor, int weight)
{
    double kaloria=0;
     if(gender.equals("male"))
     {
         if(kor>=11 && kor<=18) {

             kaloria=17.5* weight + 651;

         }
         else if(kor>=19 && kor<=30) {

             kaloria=15.3* weight + 679;

         }
         else if(kor>=31 && kor<=60) {

             kaloria=11.6* weight + 879;

         }
     }
     else if(gender.equals("female"))
     {
         if(kor>=11 && kor<=18) {

             kaloria=12.2* weight + 746;

         }
         else if(kor>=19 && kor<=30) {

             kaloria=14.7* weight + 496;

         }
         else if(kor>=31 && kor<=60) {

             kaloria=8.7* weight + 829;

         }
     }
     return kaloria;


}


}

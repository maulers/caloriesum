package hu.uniobuda.nik.caloriecounter;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;


public class ProfileFragment extends android.support.v4.app.Fragment {

    private DatabaseReference dbr;

    private int dobYear;
    private int dobMonth;
    private int dobDay;
    private AppCompatTextView textHeight;
    private AppCompatTextView textWeight;
    private AppCompatTextView textDateOfBirth;
    private AppCompatTextView dateOfBirth;
    private AppCompatSpinner heightTypes;
    private AppCompatEditText height;
    private AppCompatSpinner weightTypes;
    private AppCompatEditText weight;
    private AppCompatSpinner genderTypes;
    private AppCompatButton modifyButton;
    private ArrayAdapter<CharSequence> spinnerWeightTypesArrayAdapter;
    private ArrayAdapter<CharSequence> spinnerHeightTypesArrayAdapter;
    private ArrayAdapter<CharSequence> spinnerGenderTypesArrayAdapter;
    private DatePickerDialog.OnDateSetListener dDateSetListener;


    private UserActivity act;

    @Override
    public void onResume() {
        super.onResume();
        setDefaultValues();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile,container,false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        act = (UserActivity) getActivity();

        textHeight = view.findViewById(R.id.textHeight);
        textDateOfBirth = view.findViewById(R.id.textDateOfBirth);
        textWeight = view.findViewById(R.id.textHeight);

        height = view.findViewById(R.id.setHeight);
        heightTypes = view.findViewById(R.id.heightTypes);
        dateOfBirth = view.findViewById(R.id.setDateOfBirth);
        weightTypes = view.findViewById(R.id.weightTypes);
        weight = view.findViewById(R.id.setWeight);
        genderTypes= view.findViewById(R.id.setgender);
        modifyButton = view.findViewById(R.id.profileSetButton);
        spinnerWeightTypesArrayAdapter = ArrayAdapter.createFromResource(act,R.array.weightTypes,R.layout.support_simple_spinner_dropdown_item);
        spinnerHeightTypesArrayAdapter = ArrayAdapter.createFromResource(act,R.array.heightTypes,R.layout.support_simple_spinner_dropdown_item);
        spinnerGenderTypesArrayAdapter= ArrayAdapter.createFromResource(act,R.array.genderTypes,R.layout.support_simple_spinner_dropdown_item);
        weightTypes.setAdapter(spinnerWeightTypesArrayAdapter);
        heightTypes.setAdapter(spinnerHeightTypesArrayAdapter);
        genderTypes.setAdapter(spinnerGenderTypesArrayAdapter);

        dbr = FirebaseDatabase.getInstance().getReference("userdatas/"+act.getFba().getCurrentUser().getUid()+"/profile");

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                act.setUserProfile(dataSnapshot.getValue(UserProfile.class));
                setDefaultValues();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        act,
                        R.style.Theme_AppCompat_Light_Dialog_MinWidth,
                        dDateSetListener,
                        year,
                        month,
                        day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.GRAY));
                datePickerDialog.show();
            }
        });
        dDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                String date = setDateFormat(year,month,day);
                dobDay = day;
                dobMonth = month;
                dobYear = year;
                dateOfBirth.setText(date);
            }
        };

        modifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserProfileValues();
                dbr.setValue(act.getUserProfile());
            }
        });
    }

    private void setUserProfileValues(){
        if(!TextUtils.isEmpty(height.getText().toString()))
            act.getUserProfile().setHeight(Float.parseFloat(height.getText().toString()));
        if(!TextUtils.isEmpty(weight.getText().toString()))
            act.getUserProfile().setWeight(Float.parseFloat(weight.getText().toString()));
        if(dobYear != 0 && dobMonth != 0 && dobDay != 0){
            act.getUserProfile().setDobDay(dobDay);
            act.getUserProfile().setDobMonth(dobMonth);
            act.getUserProfile().setDobYear(dobYear);
        }
        if(!TextUtils.isEmpty(weightTypes.getSelectedItem().toString()))
            act.getUserProfile().setWeightType(weightTypes.getSelectedItem().toString());
        if(!TextUtils.isEmpty(heightTypes.getSelectedItem().toString()))
            act.getUserProfile().setHeightType(heightTypes.getSelectedItem().toString());
        if(!TextUtils.isEmpty(genderTypes.getSelectedItem().toString()))
            act.getUserProfile().setGender(genderTypes.getSelectedItem().toString());

    }
    private void setDefaultValues(){


        dobDay = act.getUserProfile().getDobDay();
        dobMonth = act.getUserProfile().getDobMonth();
        dobYear = act.getUserProfile().getDobYear();
        dateOfBirth.setText(setDateFormat(dobYear,dobMonth,dobDay));
        height.setText(Float.toString(act.getUserProfile().getHeight()));
        weight.setText(Float.toString(act.getUserProfile().getWeight()));
        heightTypes.setSelection(spinnerHeightTypesArrayAdapter.getPosition(act.getUserProfile().getHeightType()));
        weightTypes.setSelection(spinnerWeightTypesArrayAdapter.getPosition(act.getUserProfile().getWeightType()));
        genderTypes.setSelection(spinnerGenderTypesArrayAdapter.getPosition(act.getUserProfile().getGender()));
    }
    private String setDateFormat(int year, int month, int day){
        return month + "/" + day + "/" + year;
    }
}

package hu.uniobuda.nik.caloriecounter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    protected FragmentManager fm = getSupportFragmentManager();

    protected void loadMainActivityFragment(int viewID, String addBackStackClassName, Fragment fragment){
        FragmentTransaction ft = fm.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(addBackStackClassName);
        ft.replace(viewID, fragment);
        ft.commit();
    }
}

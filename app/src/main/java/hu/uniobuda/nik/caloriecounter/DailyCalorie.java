package hu.uniobuda.nik.caloriecounter;

public class DailyCalorie {
    private String dailyDate;
    private float calorie;
    private String key;

    public int getDay() {
        return day;
    }

    int day;

    public DailyCalorie(){

    }

    public DailyCalorie(String dailyDate, float calorie, String key, int day) {
        this.dailyDate = dailyDate;
        this.calorie = calorie;
        this.key = key;
        this.day = day;
    }

    public String getDailyDate() {
        return dailyDate;
    }

    public float getCalorie() {
        return calorie;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}

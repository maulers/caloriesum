package hu.uniobuda.nik.caloriecounter;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;


import com.google.firebase.auth.FirebaseAuth;


public class UserActivity extends BaseActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle mToggle;

    private UserProfile userProfile;
    private FirebaseAuth fba;

    public FirebaseAuth getFba() {
        return fba;
    }


    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        fba = FirebaseAuth.getInstance();
        userProfile = new UserProfile();
        drawerLayout = findViewById(R.id.useract_drawerLayout);
        navigationView = findViewById(R.id.navigation_menu);
        mToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupDrawerContent(navigationView);
        loadProfileFragment();
    }


    public void loadProfileFragment(){
        loadMainActivityFragment(R.id.MainPage,ProfileFragment.class.getName(),new ProfileFragment());
    }
    public void loadAddCalorieFragment(){
        loadMainActivityFragment(R.id.MainPage,AddCalorieFragment.class.getName(),new AddCalorieFragment());
    }
    public void loadTrainingPlanFragment(){
        loadMainActivityFragment(R.id.MainPage,TrainingPlan.class.getName(),new TrainingPlan());
    }
    public void loadStatisticsFragment(){
        loadMainActivityFragment(R.id.MainPage,statistics.class.getName(),new statistics());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);

    }
    private void selectFragmentFromNavigation(MenuItem item){
        switch(item.getItemId()){
            case R.id.nav_profile:
                loadProfileFragment();
                break;
            case R.id.nav_addCalorie:
                loadAddCalorieFragment();
                break;
            case R.id.nav_trainingplan:
                loadTrainingPlanFragment();
                break;
            case R.id.nav_statistics:
                loadStatisticsFragment();
                break;
            default:
                loadProfileFragment();
                break;

        }
    }

    private void setupDrawerContent(NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragmentFromNavigation(item);
                return true;
            }
        });
    }
}
